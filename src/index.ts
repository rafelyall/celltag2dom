import {
  JupyterFrontEnd,
  JupyterFrontEndPlugin
} from '@jupyterlab/application';
import {
//  NotebookActions,
  INotebookTracker
} from '@jupyterlab/notebook';
import {
    toArray
} from '@lumino/algorithm';


function turnOnTags(tracker: INotebookTracker,toggle: boolean){
    var cells = toArray(tracker.currentWidget.model.cells);
    for (var i=0;i<cells.length;i++){
        let tags=cells[i].metadata.get("tags") as string[]
        if (tags != null){
            for (var j=0;j<tags.length;j++){
                if (tags[j].startsWith("dom-") == true){
                    let newtag = tags[j].substring(4,tags[j].length)
                    // find the cell with this ID
                    let cell = tracker.currentWidget.content.widgets.find(widget => widget.model.id === cells[i].id);
                    toggle ? cell.addClass(newtag) : cell.removeClass(newtag)
                }
            }
        }
    }   
}


const extension: JupyterFrontEndPlugin<void> = {
  id: 'celltag2DOM',
  autoStart: true,
  requires: [INotebookTracker],
  activate: (app: JupyterFrontEnd, tracker: INotebookTracker) => {
    console.log('JupyterLab extension celltag2DOM is activated!');
    
    // dom-tags-on command 
    const tagsOn = 'dom-tags-on';
    app.commands.addCommand(tagsOn, {
      execute: () => {
          console.log("Executing dom-tags-on command")
          turnOnTags(tracker,true)
      }
    });
    app.commands.addKeyBinding({
      command: 'dom-tags-on',
      args: {},
      keys: ['Ctrl Y'],
      selector: '.jp-Notebook'
    });

    // dom-tags-off command 
    const tagsOff = 'dom-tags-off';
    app.commands.addCommand(tagsOff, {
      execute: () => {
          console.log("Executing dom-tags-off command")
          turnOnTags(tracker,false)
      }
    });
    app.commands.addKeyBinding({
      command: 'dom-tags-off',
      args: {},
      keys: ['Ctrl Shift Y'],
      selector: '.jp-Notebook'
    });

      
    }
};

export default extension;


